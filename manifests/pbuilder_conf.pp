# $name is the distro
define su_debuild::pbuilder_conf(
  String   $distribution             = undef,
  Enum['debian', 'ubuntu']
           $debian_type              = undef,
  Boolean  $include_stanford_keyring = true,
  Boolean  $include_backports        = true,
) {

  if ($include_stanford_keyring) {
    $conf_file_path = "/etc/pbuilder/${distribution}"
  } else {
    $conf_file_path = "/etc/pbuilder/${distribution}-no-stanford-keyring"
  }

  case $debian_type {
    'debian': {
      if ($distribution == 'sid') {
        $erb_file_path = 'su_debuild/etc/pbuilder/sid.erb'
      } else {
        $erb_file_path = 'su_debuild/etc/pbuilder/debian.erb'
      }
    }
    'ubuntu': {
      $erb_file_path = 'su_debuild/etc/pbuilder/ubuntu.erb'
    }
    default: {
      crit("do not understand '${debian_type}'")
    }
  }

  if ($distribution == 'sid') {
    file { $conf_file_path:
      content => template($erb_file_path),
      require => Package['pbuilder'],
    }
  } else {
    file { $conf_file_path:
      content => template($erb_file_path),
      require => Package['pbuilder'],
    }
  }

}
