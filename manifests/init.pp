# Configurations for a Debian build server. Does *not* setup any chroot
# environments.  For that you need to use su_debuild::chroot.
#
# Furthermore, sudo...

class su_debuild (
  Enum['root', 'all'] $cowbuild_group = 'all',
) {

  # chroots go in /var/cache/pbuilder
  file { '/var/cache/pbuilder':
    ensure => 'directory',
  }
  file { '/var/cache/pbuilder/aptcache':
    ensure  => 'directory',
    require => File['/var/cache/pbuilder'],
  }



  ### PACKAGES ###
  # Pin some packages to Debian jessie-backports.
  file {
    '/etc/apt/preferences.d/debian-build':
      ensure  => present,
      content => template('su_debuild/etc/apt/preferences.d/debian-build.erb'),
      mode    => '0644',
      notify  => Exec['apt-get_update_debuild_jessie'];
  }

  # Triggered to refresh local package lists.
  exec { 'apt-get_update_debuild_jessie':
    command     => 'apt-get update',
    refreshonly => true,
  }

  # Packages needed for package building. Add ant here because although we
  # don't need it in general on all development systems, but it's useful
  # for some Debian packages. We don't install reprepro in case it is
  # installed via su_debian_archive.
  $packages_to_install = [
    'alien',
    'ant',
    'apt-file',
    'build-essential',
    'dash',
    'debhelper',
    'debmake',
    'devscripts',
    'devscripts-el',
    'dh-autoreconf',
    'dh-make',
    'dh-python',
    'diffstat',
    'dkms',
    'dpkg-dev-el',
    'dput',
    'fakeroot',
    'gem2deb',
    'git',
    'git-buildpackage',
    'gitk',
    'gnupg',
    'javahelper',
    'libcurl4-openssl-dev',
    'libkrb5-dev',
    'libdistro-info-perl',
    'libmodule-build-perl',
    'libparse-debcontrol-perl',
    'libremctl-dev',
    'libssl-dev',
    'lintian',
    'manpages-dev',
    'module-assistant',
    'openafs-modules-source',
    'patchutils',
    'php-cli',
    'pinentry-curses',
    'pristine-tar',
    'quilt',
    'reportbug',
    'w3m',
  ]

  ensure_packages($packages_to_install, {'ensure' => 'present'})

  package { 'gnupg-agent':
    ensure  => installed,
    require => Package['pinentry-curses'];
  }

  # Distribution-specific packages.
  # maven: maven2 only supported on wheezy and jessie (EOL'ed in 2014)
  case ($::lsbdistcodename) {
    'jessie': {
      package {
        'apache2-dev': ensure => installed;
        'maven2':      ensure => installed;
      }
    }
    default: {
      package {
        'apache2-dev': ensure => installed;
        'maven':       ensure => installed;
      }
    }
  }

  ## Install Ubuntu keyrings
  include su_debuild::ubuntu_keyring

  # Additional packages to build kernel modules.
  case $::lsbdistcodename {
    'jessie': {
      package {
        'linux-headers-3.16.0-4-all': ensure => installed;
      }
    }
    default: {
      package {
        'linux-headers-amd64': ensure => installed;
      }
    }
  }

  # Configuration and cache directory for Maven.
  if ($::lsbdistcodename == 'jessie') {
    # MAVEN2
    file {
      '/etc/maven2/settings.xml':
        source  => 'puppet:///modules/su_debuild/etc/maven2/settings.xml',
        require => Package['maven2'];
      '/srv/maven':
        ensure  => directory,
        mode    => '2775';
    }
  } else {
    # MAVEN3
    file {
      '/etc/maven/settings.xml':
        source  => 'puppet:///modules/su_debuild/etc/maven/settings.xml',
        require => Package['maven'];
      '/srv/maven':
        ensure  => directory,
        mode    => '2775';
    }
  }

  # Add a deb-src configuration for unstable so that we can easily download
  # packages for inspection and backporting.
  file { '/etc/apt/sources.list.d/src-sid.list':
    source => 'puppet:///modules/su_debuild/etc/apt/sources.list.d/src-sid.list',
  }


  ### PBUILDER ###

  # The pbuild scripts are in stanford-server-debuild
  package { 'stanford-server-debuild':
    ensure => installed,
  }

  # Basic configuration for pbuilder. Note that pbuild and pbuilder
  # are not useful without chroot environments (see su_debuild::chroots).
  file { '/etc/pbuilderrc':
    source => 'puppet:///modules/su_debuild/etc/pbuilderrc',
  }

  # Cron job to update the apt-file database nightly.
  file { '/etc/cron.daily/apt-file-update':
    source => 'puppet:///modules/su_debuild/etc/cron.daily/apt-file-update',
    mode   => '0755',
  }

  # Create a /etc/sudoers.d for running cowbuilder
  case $cowbuild_group {
    'root': {
      $sudo_cowbuild_group = '%root'
      $sudo_cowbuild_text  = 'Members of the root group'
    }
    'all':  {
      $sudo_cowbuild_group = 'ALL'
      $sudo_cowbuild_text  = 'All users'
    }
    default:  {
      crit("unknown cowbuild_group '${cowbuild_group}'")
    }
  }

  sudo::conf { 'cowbuilder':
      priority => 50,
      content  => [
                    "# ${sudo_cowbuild_text} can run cowbuilder (needed for building Debian packages)",
                    "${sudo_cowbuild_group} ALL = NOPASSWD: /usr/sbin/cowbuilder",
                  ],
  }
}
