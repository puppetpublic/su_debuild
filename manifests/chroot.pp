# This file handles configuration of a pbuilder chroot for a Debian package
# build system.  The public interface for this file is the define specified at
# the very top.  This sets up a chroot and build environment for a specified
# distribution.
#
# This define only partly supports ensure to the extent of not doing any work
# if ensure isn't set to present.  It doesn't, however, support removing
# configuration on ensure => absent.
#
# $distribution: normally the chroot uses $name as the Debian distribution target for the
# chroot. If you need to override this you can use $distribution.

define su_debuild::chroot(
  Enum['present', 'absent']
                   $ensure            = undef,
  Optional[String] $distribution      = undef,
  String           $debian_type       = undef,
  Boolean          $include_backports = true,
) {

  $distribution_real = $distribution ? {
    undef   => $name,
    default => $distribution
  }

  if ($ensure == 'present') {
    $path    = "/var/cache/pbuilder/base-${name}.cow"
    $config  = "/etc/pbuilder/${name}-no-stanford-keyring"
    $options = "--distribution ${distribution_real} --configfile ${config}"

    # We need two versions of each pbuilder configuration file: one
    # with stanford-keyring and one without.
    su_debuild::pbuilder_conf { "${distribution_real}-with-stanford-keyring":
      distribution             => $distribution_real,
      debian_type              => $debian_type,
      include_backports        => $include_backports,
      include_stanford_keyring => true,
    }
    su_debuild::pbuilder_conf { "${distribution_real}-without-stanford-keyring":
      distribution             => $distribution_real,
      debian_type              => $debian_type,
      include_backports        => $include_backports,
      include_stanford_keyring => false,
    }

    # If this is an Ubuntu distribution, link the distribution name to
    # "gutsy".
    if ($distribution_real =~ /trusty|xenial|bionic|focal|jammy/) {
      file { "/usr/share/debootstrap/scripts/${distribution_real}":
        ensure => 'link',
        target => '/usr/share/debootstrap/scripts/gutsy',
      }
    }

    # There is a chicken-and-egg problem in getting the stanford
    # repository's public key installed.  We work around this as follows:
    #
    #  1. Install the chroot WITHOUT the stanford-keyring.
    #  2. Copy the Stanford Debian keyring to the new chroot.
    #  3. Copy the correct distribution's sources.list file to the just-created
    #     /root directory in the chroot directory.
    #  4. Copy the correct distribution's sources.list file from the
    #     /root directory to the chroot's apt directgory, apt-get update, and
    #     install stanford-keyring package.
    #
    #  We break things up into #3 and #4 so that we can be sure that deleting
    #  the chroot directory will ensure it comes back again correctly.

    # 1. Create the chroot WITHOUT the stanford-keyring.
    exec { "cowbuilder --create ${name}":
      path    => '/usr/sbin:/usr/bin:/bin',
      command => "cowbuilder --create --basepath ${path} ${options}",
      creates => $path,
      require => [ Package['cowdancer'],
        File['/etc/pbuilderrc'],
        File["/etc/pbuilder/${name}-no-stanford-keyring"] ],
    }

    # 2. Copy the stanford keyring file from the host computer.
    file { "${path}/etc/apt/trusted.gpg.d/stanford-keyring.gpg":
        ensure  => present,
        source  => '/usr/share/keyrings/stanford-keyring.gpg',
        require => Exec["cowbuilder --create ${name}"];
    }

    # 3. Install the correct sources.list to ${path}/root/ so we can use
    # it later.
    file { "${path}/root/sources.list.${name}":
      source  => "puppet:///modules/su_debuild/etc/apt/sources.list.${name}",
      require => Exec["cowbuilder --create ${name}"],
      notify  => Exec["install stanford-keyring in ${path} chroot"],
    }

    # 4. Install stanford-keyring. Be sure to run "apt-get update" first.
    # Only run if /usr/share/keyrings/stanford-keyring.gpg (in chroot)
    # does _not_ already exist and only on notification by #3.
    $exec_cmd = @("EXEC_CMD"/L)
      cp ${path}/root/sources.list.${name} ${path}/etc/apt/sources.list; \
      chroot ${path} apt-get update; \
      chroot ${path} apt-get install stanford-keyring\
      | EXEC_CMD

    exec { "install stanford-keyring in ${path} chroot":
      path        => '/usr/sbin:/usr/bin:/bin:/sbin',
      refreshonly => true,
      command     => $exec_cmd,
      require     => File["${path}/etc/apt/trusted.gpg.d/stanford-keyring.gpg"],
    }
  } else {
    # ABSENT
    notify { 'absent not done yet': }
  }

}
