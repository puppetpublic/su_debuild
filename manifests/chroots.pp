class su_debuild::chroots (
 $chroots = [],
) {

  # Don't do anything if $chroots is empty.
  if (length($chroots) > 0) {
    package {
      'debootstrap': ensure  => installed;
      'pbuilder':    ensure  => installed;
    }

    # Make sure /etc/pbuild is exists
    file { '/etc/pbuilder':
      ensure => directory,
    }

    package { 'cowdancer':
      ensure  => installed,
      require => Package['debootstrap'];
    }

    # Define the environments we know about.
    $code_to_debian_type = {
      'sid'      =>  'debian',
      'bookworm' =>  'debian',
      'bullseye' =>  'debian',
      'buster'   =>  'debian',
      'stretch'  =>  'debian',
      'jessie'   =>  'debian',
      'wheezy'   =>  'debian',
      'jammy'    =>  'ubuntu',
      'focal'    =>  'ubuntu',
      'bionic'   =>  'ubuntu',
      'xenial'   =>  'ubuntu',
      'trusty'   =>  'ubuntu',
    }

    # Set up the chroots.
    $chroots.each |String $chroot| {
      su_debuild::chroot {$chroot:
        ensure      => present,
        debian_type => $code_to_debian_type[$chroot],
      }
    }

    # Cron job to update the pbuilder chroots nightly.
    file { '/etc/cron.daily/pbuilder-update':
      content => template('su_debuild/etc/cron.daily/pbuilder-update.erb'),
      mode    => '0755',
    }
  }
}
