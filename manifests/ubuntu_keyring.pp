# This class installs the ubuntu package repository keyring. These keys
# are needed so that the process that creates the chroot directories in
# /var/cache/pbuild can download packages from the Ubuntu package
# repositories.
#
# It is safe to include this class even if you don't build any Ubuntu
# chroots.

class su_debuild::ubuntu_keyring {

  case $::lsbdistcodename {
    # This package is not (yet) in bullseye, so we only install it for
    # buster, stretch, or sid.
    'buster', 'stretch', 'sid': {
      package {
        'ubuntu-archive-keyring': ensure => installed;
      }
    }
    # Debian bullseye and bookworm do not include any ubuntu-keyring packages so we
    # install the needed GPG key file directly.
    'bullseye', 'bookworm': {
      file { '/usr/share/keyrings/ubuntu-archive-keyring.gpg':
        ensure => present,
        source => 'puppet:///modules/su_debuild/usr/share/keyrings/ubuntu-archive-keyring.gpg',
        mode   => '0644',
      }
    }
    default: { fail("do not know how to handle ${::lsbdistcodename}") }
  }

}
