# The su_debuild Puppet module

## Overview

The `su_debuild` Puppet classes will set up a complete Debian build
system on your Debian server. This includes chroots to allow for the use
of `pbuilder` and `gbp buildpackage`.

## Distributions supported

The following Debian distributions are supported:

* wheezy
* jessie
* stretch
* buster

The following Ubuntu distributions are supported:

* trusty
* xenial
* bionic
* focal (coming soon)


## Installing the necessary packages

If all you need are the packages and not the chroot environments, do this:

    include su_debian

Note that this will allow you to run most of the Debian packaging commands
including `dpkg-buildpackage` but not any of the commands that require a
chroot.

## Installing chroot environments

To install chroot environments use the `su_debuild::chroots` class. For
example, the following will setup the necessary packages and install
chroot environments for Debian `sid`, `stretch`, `jessie`, and Ubuntu
`bionic`:

    include su_debuild
    class { 'su_debuild::chroots':
      chroots => [ 'sid', 'stretch', 'jessie', 'bionic']
    }

The above also installs a daily cron job to update the chroots.

## Using sudo

In order to run `pbuilder` as a non-root user you need to tell sudo that
all users who are members of the `root` group can run cowbuilder.  The
`su_debuild` module sets this up in sudo for you automatically by adding a
file to `/etc/sudoers.d` containing this line:

    %root    ALL = NOPASSWD: /usr/sbin/cowbuilder
